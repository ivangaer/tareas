﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class GameOver : MonoBehaviour
{
    
    public static bool JugadorMuerto = false;
    private Text gameOver;
    void Start()
    {
        gameOver = GetComponent<Text>();
        gameOver.enabled = false;
    }

    
    void Update()
    {
        if (JugadorMuerto)
        {
            Time.timeScale = 0;
            gameOver.enabled = true;
        }
    }
}
