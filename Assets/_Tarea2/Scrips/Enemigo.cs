﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    [SerializeField] float speed;
    private bool direccionHorizontal = false;
    private float fireRate = 0.997f;
    [SerializeField] public GameObject shot;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bala")
        { 
            transform.localScale -= Vector3.Scale(transform.localScale, new Vector3(0.2f, 0.2f, 0));
        }
    }
    void Update()
    {
        if (Random.value > fireRate)
        {
            Instantiate(shot, transform.position, transform.rotation);
        }
        if (transform.position.y <= 4)
        {
            GameOver.JugadorMuerto = true;
            Time.timeScale = 0;
        }
        if (!direccionHorizontal)
        {
            if (transform.position.x <= 9)
            {
                Vector3 currentPosition = transform.position;
                currentPosition.x += 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                direccionHorizontal = true;
                transform.position += Vector3.down;
            }
        }
        else
        {
            if (transform.position.x >= -6.5)
            {
                Vector3 currentPosition = transform.position;
                currentPosition.x -= 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                direccionHorizontal = false;
                transform.position += Vector3.down;
            }
        }

    }
}
