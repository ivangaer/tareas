﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    private Controles Controles;

    private void Awake()
    {
        Controles = new Controles();
    }

    private void OnEnable()
    {
        Controles.Enable();
    }
    private void OnDisable()
    {
        Controles.Disable();
    }
    void Start()
    {
        Controles.NaveAD.Restart.performed += _ => Reinicio();
    }
    void Reinicio()
    {
        PuntajeDelJugador.PuntajePrin = 0;
        GameOver.JugadorMuerto = false;
        Time.timeScale = 1;

        SceneManager.LoadScene ("SpaceInvaders");
    }

}
