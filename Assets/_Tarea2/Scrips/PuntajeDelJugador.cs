﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntajeDelJugador : MonoBehaviour
{
    public static float PuntajePrin = 0;
    private Text textpuntaje;
    // Start is called before the first frame update
    void Start()
    {
        textpuntaje = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        textpuntaje.text = "Puntaje : " + PuntajePrin;
    }
}
