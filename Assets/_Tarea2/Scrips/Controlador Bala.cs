﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorBala : MonoBehaviour
{

	private Transform bullet;
	[SerializeField] public float speed;

	// Use this for initialization
	void Start()
	{
		bullet = GetComponent<Transform>();
	}

	void FixedUpdate()
	{
		bullet.position += Vector3.up * speed;

		if (bullet.position.y >= 6)
			Destroy(gameObject);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Enemigo")
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
		else if (other.tag == "Nave")
			Destroy(gameObject);
	}
}
