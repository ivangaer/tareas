﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Win : MonoBehaviour
{
    [SerializeField] Text winText;
    [SerializeField] private Transform Enemigo;
    [SerializeField] private Transform Enemigo1;
    void Start()
    {
        winText.enabled = false;
    }

    void Update()
    {
        if (Enemigo1.childCount == 0 && Enemigo.childCount == 0)
        {
            winText.enabled = true;
        }
    }
}
