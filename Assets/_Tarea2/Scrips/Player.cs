﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Controles Controles;
    [SerializeField] public float speed = 5f;
    [SerializeField] public float limin, limax;

    [SerializeField] public GameObject Tiro;
    [SerializeField] public Transform SpawnTiro;
    [SerializeField] public float fireRate;
    public Slider timerSlider;
    private float nextFire;

    private void Awake()
    {
        Controles = new Controles();
    }

    private void OnEnable()
    {
        Controles.Enable();
    }
    private void OnDisable()
    {
        Controles.Disable();
    }
    private void Start()
    {
        Controles.NaveAD.Disp.performed += _ => Disparo();
        timerSlider.maxValue = fireRate;
        timerSlider.value = fireRate;
        nextFire = fireRate;
    }
    private void Disparo()
    {
        if(nextFire <= 0)
        {
            nextFire = fireRate;
            Instantiate(Tiro, SpawnTiro.position, SpawnTiro.rotation);

            FindObjectOfType<AudioManager>().Play("Disparo");
        }

    }
    private void Update()
    {
        float movementInput = Controles.NaveAD.Move.ReadValue<float>();
        if (transform.position.x >= limin && transform.position.x <= limax)
        {
            Vector3 currentPosition = transform.position;
            currentPosition.x += movementInput * speed * Time.deltaTime;
            transform.position = currentPosition;
        }
        if (transform.position.x < limin)
        {
            Vector3 currentPosition = transform.position;
            currentPosition.x = limin;
            transform.position = currentPosition;
        }
        if (transform.position.x > limax)
        {
            Vector3 currentPosition = transform.position;
            currentPosition.x = limax;
            transform.position = currentPosition;
        }

        nextFire -= Time.deltaTime;
        if (nextFire < 0)
        {
            nextFire = 0;
        }
        timerSlider.value = nextFire;
    }


}