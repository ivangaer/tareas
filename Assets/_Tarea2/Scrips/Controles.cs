// GENERATED AUTOMATICALLY FROM 'Assets/_Tarea2/Scrips/Controles.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controles : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controles()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controles"",
    ""maps"": [
        {
            ""name"": ""Nave AD"",
            ""id"": ""32524301-d088-4516-9418-957dc2c117dd"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ab5226eb-1ab7-4de1-9add-cbc2ecb30628"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Disp"",
                    ""type"": ""Button"",
                    ""id"": ""8db4ec96-0b8e-495b-8716-74ae88f8a6f5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""41d95d86-a40d-4413-9897-8bf09c05c4ef"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Strafe"",
                    ""id"": ""b053546c-1eed-4d3d-ba43-1b09db81500b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c2a40fd7-d447-4549-801f-bd32df9cca72"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""eead90d8-11ec-41ef-befc-6575e444613a"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""16938e55-e016-4e5b-ac9c-a9fbd3c2fa16"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Disp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""27881197-b543-4334-aa57-5c4e1af08a72"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Nave AD
        m_NaveAD = asset.FindActionMap("Nave AD", throwIfNotFound: true);
        m_NaveAD_Move = m_NaveAD.FindAction("Move", throwIfNotFound: true);
        m_NaveAD_Disp = m_NaveAD.FindAction("Disp", throwIfNotFound: true);
        m_NaveAD_Restart = m_NaveAD.FindAction("Restart", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Nave AD
    private readonly InputActionMap m_NaveAD;
    private INaveADActions m_NaveADActionsCallbackInterface;
    private readonly InputAction m_NaveAD_Move;
    private readonly InputAction m_NaveAD_Disp;
    private readonly InputAction m_NaveAD_Restart;
    public struct NaveADActions
    {
        private @Controles m_Wrapper;
        public NaveADActions(@Controles wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_NaveAD_Move;
        public InputAction @Disp => m_Wrapper.m_NaveAD_Disp;
        public InputAction @Restart => m_Wrapper.m_NaveAD_Restart;
        public InputActionMap Get() { return m_Wrapper.m_NaveAD; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(NaveADActions set) { return set.Get(); }
        public void SetCallbacks(INaveADActions instance)
        {
            if (m_Wrapper.m_NaveADActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_NaveADActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_NaveADActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_NaveADActionsCallbackInterface.OnMove;
                @Disp.started -= m_Wrapper.m_NaveADActionsCallbackInterface.OnDisp;
                @Disp.performed -= m_Wrapper.m_NaveADActionsCallbackInterface.OnDisp;
                @Disp.canceled -= m_Wrapper.m_NaveADActionsCallbackInterface.OnDisp;
                @Restart.started -= m_Wrapper.m_NaveADActionsCallbackInterface.OnRestart;
                @Restart.performed -= m_Wrapper.m_NaveADActionsCallbackInterface.OnRestart;
                @Restart.canceled -= m_Wrapper.m_NaveADActionsCallbackInterface.OnRestart;
            }
            m_Wrapper.m_NaveADActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Disp.started += instance.OnDisp;
                @Disp.performed += instance.OnDisp;
                @Disp.canceled += instance.OnDisp;
                @Restart.started += instance.OnRestart;
                @Restart.performed += instance.OnRestart;
                @Restart.canceled += instance.OnRestart;
            }
        }
    }
    public NaveADActions @NaveAD => new NaveADActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface INaveADActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnDisp(InputAction.CallbackContext context);
        void OnRestart(InputAction.CallbackContext context);
    }
}
