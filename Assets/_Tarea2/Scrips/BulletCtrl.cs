﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BulletCtrl : MonoBehaviour
{

	private Transform bullet;
	[SerializeField] public float speed;

	// Use this for initialization
	void Start()
	{
		bullet = GetComponent<Transform>();
	}

	void FixedUpdate()
	{
		bullet.position += Vector3.up * speed;

		if (bullet.position.y >= 10)
			Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Enemigo")
		{
			GameObject playerBase = other.gameObject;
			BaseHealth baseHealth = playerBase.GetComponent<BaseHealth>();
			baseHealth.health -= 1;
			Destroy(gameObject);
			PuntajeDelJugador.PuntajePrin += 10;
			FindObjectOfType<AudioManager>().Play("Ataque");

		}
		if (other.tag == "Enemigo1")
        {
			GameObject playerBase = other.gameObject;
			BaseHealth baseHealth = playerBase.GetComponent<BaseHealth>();
			baseHealth.health -= 1;
			Destroy(gameObject);
			PuntajeDelJugador.PuntajePrin += 10;
			FindObjectOfType<AudioManager>().Play("Ataque");
		}
	}
}
