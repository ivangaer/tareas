﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    [SerializeField] float speed;
    private bool direccionHorizontal = false;

    void Start()
    {

    }
    void Update()
    {


        if (transform.position.y <= 4)
        {
            GameOver.JugadorMuerto = true;
            Time.timeScale = 0;
        }
        if (!direccionHorizontal)
        {
            if (transform.position.x <= 9)
            {
                Vector3 currentPosition = transform.position;
                currentPosition.x += 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                transform.position += Vector3.down;
                direccionHorizontal = true;
            }
        }
        else
        {
            if (transform.position.x >= -6.5)
            {
                Vector3 currentPosition = transform.position;
                currentPosition.x -= 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                transform.position += Vector3.down;
                direccionHorizontal = false;
            }
        }

    }
}
