﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemiga : MonoBehaviour
{

	private Transform bullet1;
	public float speed;

	// Use this for initialization
	void Start()
	{
		bullet1 = GetComponent<Transform>();
	}

	void FixedUpdate()
	{
		bullet1.position += Vector3.up * -speed;

		if (bullet1.position.y <= -10)
			Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
			GameOver.JugadorMuerto = true;
			FindObjectOfType<AudioManager>().Play("Muerto");
		}
		if (other.tag == "Cobertura")
		{
			Destroy(this.gameObject);
		}
	}
}
