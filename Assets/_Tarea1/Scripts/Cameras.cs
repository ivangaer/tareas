﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Cameras : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera[] listCameras;
    private int _currentCamera = 0;

    void Start()
    {
        ResetPriority(); //Resetea las prioridades de las camaras a 0
        listCameras[0].Priority = 1;
        StartCoroutine(ChangeCamera()); //Ejecuta la corrutina
    }

    private void ResetPriority()
    {
        for (int i = 0; i < listCameras.Length; i++)
        {
            listCameras[i].Priority = 0;
        }
    }

    IEnumerator ChangeCamera()
    {
        yield return new WaitForSeconds(2f); //Interrumpe el ciclo por un segundo
        ResetPriority();
        listCameras[_currentCamera].Priority = 1;
        _currentCamera++;

        if (_currentCamera == listCameras.Length)
        {
            _currentCamera = 0;
        }

        StartCoroutine(ChangeCamera());
    }


}
