﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameras2 : MonoBehaviour
{
    public GameObject CV1;
    public GameObject CV2;
    public GameObject CV3;
    public GameObject CV4;
    void Update()
    {
       if (Input.GetKey(KeyCode.Alpha1))
        {
            CV1.gameObject.SetActive(true);
            CV2.gameObject.SetActive(false);
            CV3.gameObject.SetActive(false);
            CV4.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            CV1.gameObject.SetActive(false);
            CV2.gameObject.SetActive(true);
            CV3.gameObject.SetActive(false);
            CV4.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            CV1.gameObject.SetActive(false);
            CV2.gameObject.SetActive(false);
            CV3.gameObject.SetActive(true);
            CV4.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            CV1.gameObject.SetActive(false);
            CV2.gameObject.SetActive(false);
            CV3.gameObject.SetActive(false);
            CV4.gameObject.SetActive(true);
        }
    }
}
