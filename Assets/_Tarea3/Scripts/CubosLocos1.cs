﻿using UnityEngine;
using System.Collections;
using DentedPixel;

public class CubosLocos1 : MonoBehaviour
{

    void Start()
    {
        GameObject avatarRotate = GameObject.Find("Cubo 1");
        GameObject avatarScale = GameObject.Find("Cubo 2");
        GameObject avatarMove = GameObject.Find("Cubo 3");

        LeanTween.rotateAround(avatarRotate, Vector3.forward, 270f, 10f);

        // Scale Example
        LeanTween.scale(avatarScale, new Vector3(1.7f, 1.7f, 1.7f), 5f).setEase(LeanTweenType.easeOutBounce);
        LeanTween.moveX(avatarScale, avatarScale.transform.position.x + 5f, 5f).setEase(LeanTweenType.easeOutBounce); // Simultaneously target many different tweens on the same object 

        // Move Example
        LeanTween.move(avatarMove, avatarMove.transform.position + new Vector3(-9f, 0f, 1f), 2f).setEase(LeanTweenType.easeInQuad);

        // Delay
        LeanTween.move(avatarMove, avatarMove.transform.position + new Vector3(-6f, 0f, 1f), 2f).setDelay(3f);

        // Chain properties (delay, easing with a set repeating of type ping pong)
        LeanTween.scale(avatarScale, new Vector3(0.2f, 0.2f, 0.2f), 1f).setDelay(7f).setEase(LeanTweenType.easeInOutCirc).setLoopPingPong(3);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
